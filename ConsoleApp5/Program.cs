﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.String;

namespace ConsoleApp5
{
    class A
    {
        public readonly int i = 9;
        public const int j = 10;

        public string FirstName { get; } = "Rachna";
        public string LastName { get; }

        public string FullName => $"{FirstName} {LastName}";

        public A(string firstName, string lastName)
        {
            if (IsNullOrWhiteSpace(lastName))
                throw new ArgumentException(message: "Cannot be blank", paramName: nameof(lastName));
            FirstName = firstName;
            LastName = lastName;
        }
        public A() { }

        public A(int i)
        {
            this.i = i;
        }

        virtual public void First()
        {
            Console.WriteLine("in First of A");
        }

        virtual public void Second()
        {
            Console.WriteLine("in Second of A");
        }
    }

    class B: A
    {
        override public void First()
        {
            Console.WriteLine("in First of B");
        }
        
        new public void Second()
        {
            Console.WriteLine("in Second of B");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            A v1 = new A();

            Console.WriteLine(v1.i + "" + A.j);

            A v2 = new A(15);

            Console.WriteLine(v2.i + "" + A.j);

            A a = new A();
            a.First();
            B b = new B();
            b.First();
            A b1 = new B();
            b1.First();
            try
            {
               // v1 = null;
                v1.First();
            }
            catch (System.NullReferenceException e) when (b1.FirstName.Equals("XYZ"))
            {
                
            }
            catch (System.Exception e) 
            {

            }
            finally
            {
                //v1.First();
            }
            b1.Second();

            //v1 = new A("Ravi", "Pandey");
            Console.WriteLine($"{v1.FirstName} {v1.LastName} Full name : {v1.FullName}");

            string mySentence = "This is an exam";

            var words = (mySentence.Split(new char[] { ' ' }).ToList());

            words.Sort();

            var str = string.Join(" " ,words.ToArray());

            Console.WriteLine(str);

            Console.ReadKey();
        }
    }
}
